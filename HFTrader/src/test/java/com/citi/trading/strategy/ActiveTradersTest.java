package com.citi.trading.strategy;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.OrderPlacer;
import com.citi.trading.TestDB;
import com.citi.trading.Trade;
import com.citi.trading.TradeRepository;
import com.citi.trading.Trade.Result;
import com.citi.trading.pricing.PricingSource;
import com.citi.trading.strategy.TraderTest.TestableTrader;

/**
 * Unit test for the {@link ActiveTraders} component. We configure mock
 * pricing and market components, but let the persistence layer run since
 * we can pipe everything to the in-memory database. Test cases cover the 
 * bindings of strategies to traders and the lifecycle management of 
 * loading, starting/stopping, and adding/removing to assure that individual
 * traders are started and stopped appropriately.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=ActiveTradersTest.Config.class)
@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class ActiveTradersTest {
	
	@Configuration
	@ComponentScan(basePackageClasses=ActiveTraders.class, excludeFilters={
			@ComponentScan.Filter(Configuration.class),
			@ComponentScan.Filter(type=FilterType.REGEX, pattern=".*TraderService")
		})
	@EnableAutoConfiguration
	@EntityScan(basePackageClasses=Trade.class)
	@EnableJpaRepositories(basePackageClasses=TradeRepository.class)
	@PropertySource("classpath:memoryDB.properties")
	public static class Config {

		@Bean()
		public TestDB testDB() {
			return new TestDB();
		}

		@Bean
		public PricingSource mockPricing() {
			return mock(PricingSource.class);
		}
		
		@Bean
		public OrderPlacer mockMarket() {
			return mock(OrderPlacer.class);
		}
	}

	@Autowired
	private TestDB testDB;
	
	@Autowired
	private PricingSource mockPricing;
	
	@Autowired
	private StrategyRepository strategyRepository;
	
	@Autowired
	private ActiveTraders activeTraders;
	
	// ActiveTraders hits the database @PostConstruct, so @Before doesn't work here
	@After
	public void setUp() throws Exception {
		testDB.reset();
	}
	
	@Test
	public void testBindings() {
		assertThat((TwoMovingAveragesTrader) activeTraders.getTraderForStrategy(new TwoMovingAverages()), 
				isA(TwoMovingAveragesTrader.class));
	}
	
	@Test
	public void testLoad() {
		List<Trader<?>> traders = activeTraders.get();
		assertThat(traders, hasSize(2));
		assertThat(traders.get(0) instanceof TwoMovingAveragesTrader, equalTo(true));
		assertThat(traders.get(1) instanceof TwoMovingAveragesTrader, equalTo(true));
	}
	
	@Test
	public void testStartAndStop() {
		activeTraders.start();
		verify(mockPricing, times(2)).subscribe(any(), eq(4), any());
		
		activeTraders.stop();
		verify(mockPricing, times(2)).unsubscribe(any(), any());
	}

	@Test
	public void testStartWhenAddedWhileTrading() {
		activeTraders.start();
		
		TwoMovingAverages twoMA = 
				new TwoMovingAverages("AA", 100, 30000, 60000, 0.03);
		strategyRepository.save(twoMA);
		activeTraders.addTraderFor(twoMA);
		verify(mockPricing, times(3)).subscribe(any(), eq(4), any());
	}

	@Test
	public void testNoStartWhenAddedWhileNotTrading() {
		TwoMovingAverages twoMA = 
				new TwoMovingAverages("AA", 100, 30000, 60000, 0.03);
		strategyRepository.save(twoMA);
		activeTraders.addTraderFor(twoMA);
		verify(mockPricing, never()).subscribe(any(), eq(4), any());
	}

	@Test
	public void testStopWhenRemovedWhileTrading() {
		activeTraders.start();
		activeTraders.removeTrader(activeTraders.get().get(1));
		verify(mockPricing).unsubscribe(eq("IBM"), any());
	}

	@Test
	public void testNoStopWhenRemovedWhileNotTrading() {
		activeTraders.removeTrader(activeTraders.get().get(1));
		verify(mockPricing, never()).unsubscribe(eq("IBM"), any());
	}
	
	@Test
	public void testNoRemovalWhenReplyNotAccepted() {
		// ??? removeTraderFor different from removeTrader
		TwoMovingAverages twoMA = 
				new TwoMovingAverages("AA", 100, 30000, 60000, 0.03);
		strategyRepository.save(twoMA);
		activeTraders.addTraderFor(twoMA);
		Trader trader = activeTraders.getTraderForStrategy(twoMA);
		trader.opener.placeOrder(true, 100);
		assertThat(activeTraders.removeTrader(trader), equalTo(false));
	}
	
	@Test
	public void testNoRemovalWhenTraderOpen() {
		TwoMovingAverages twoMA = 
				new TwoMovingAverages("AA", 100, 30000, 60000, 0.03);
		strategyRepository.save(twoMA);
		activeTraders.addTraderFor(twoMA);
		Trader trader = activeTraders.getTraderForStrategy(twoMA);
		Trade trade = new Trade("ABC", true, 100, 100);
		trade.setResult(Result.FILLED);
		trader.opener.accept(trade);
		assertThat(activeTraders.removeTrader(trader), equalTo(false));
	}
}
