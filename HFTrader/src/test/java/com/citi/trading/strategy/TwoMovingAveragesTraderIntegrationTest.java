package com.citi.trading.strategy;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.HFTrader;
import com.citi.trading.TradeRepository;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.Pricing;

/**
 * Integration test of the {@link TwoMovingAveragesTrader}, using the main
 * application configuration for live connections to pricing, market,
 * and persistence. We activate a trader on MRK, which in our pricing service
 * has a predictable, sine-wave pricing curve, and verify that over a certain
 * time it takes the expected positions.
 * 
 * For this test to run, the pricing service and mock market must be running,
 * and the latter must be run in its "alwaysfill" mode so as to be 
 * predictable. This test can take up to ten minutes, as it first 
 * waits for the right part of the five-minute sine wave to kick off
 * the trader, and then follows that five-minute cycle.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=HFTrader.class)
public class TwoMovingAveragesTraderIntegrationTest {

	public static final String STOCK = "MRK";
	public static final int SIZE = 100;

	@Autowired
	private TwoMovingAveragesTrader trader1;
	
	@Autowired
	private Pricing pricing;
	
	@Autowired
	private PositionRepository positionRepository;
	
	@Autowired
	private StrategyRepository strategyRepository;
	
	@Autowired
	private TradeRepository tradeRepository;
	
	private TwoMovingAverages twoMA1;
	
	private static double monitorOpen;
	private static double monitorClose;
	
	@Test
	@SuppressWarnings("unchecked") // Hamcrest contains() and varargs
	public void testSineWave() throws Exception{

		pricing.subscribe("MRK", 1, data -> {
			PricePoint pricePoint = data.getData(1).findAny().get();
			monitorOpen = pricePoint.getOpen();
			monitorClose = pricePoint.getClose();
		});
		System.out.println("Queueing up pricing data ...");
		while (monitorOpen != 59.6227 || monitorClose != 60.3826) {
			pricing.getPriceData();
			System.out.format("Open %1.4f close %1.4f%n", monitorOpen, monitorClose);
			Thread.sleep(15000);
		}
		System.out.println("Ready!");

		twoMA1 = new TwoMovingAverages("MRK", SIZE, 60000, 180000, 0.03);
		twoMA1 = strategyRepository.save(twoMA1);
		trader1.setStrategy(twoMA1);
		trader1.startTrading(mock(ActiveTraders.class));
		
		// Get first 12 data points, then 14 individually after that
		pricing.getPriceData();
		for (int i = 0; i < 15; ++i) {
			Thread.sleep(15000);
			pricing.getPriceData();
		}
		
		twoMA1 = (TwoMovingAverages) strategyRepository
				.findStrategyAndPositions(twoMA1.getId());
		assertThat(twoMA1.getPositions(), hasSize(2));
		assertThat(twoMA1.getPositions(), contains(
			both(isA(Position.class)).and(allOf( 
				hasProperty("openingTrade", allOf(
					hasProperty("buy", equalTo(true)),
					hasProperty("price", closeTo(62.59, 0.0001))
					)),
				hasProperty("closingTrade", allOf(
					hasProperty("buy", equalTo(false)),
					hasProperty("price", closeTo(64.7974, 0.0001))
					))
				)),
			both(isA(Position.class)).and(allOf(
				hasProperty("openingTrade", allOf(
					hasProperty("buy", equalTo(false)),
					hasProperty("price", closeTo(62.59, 0.0001))
					)),
				hasProperty("closingTrade", allOf(
					hasProperty("buy", equalTo(true)),
					hasProperty("price", closeTo(60.3826, 0.0001))
					))
				))
			));
	}
	
	@After
	public void tearDown() {
		int ID = twoMA1.getId();
		if (ID != 0) {
			TwoMovingAverages toKill = (TwoMovingAverages) 
					strategyRepository.findStrategyAndPositions(ID);
			for (Position position : toKill.getPositions()) {
				positionRepository.delete(position);
				tradeRepository.delete(position.getOpeningTrade());
				if (position.getClosingTrade() != null) {
					tradeRepository.delete(position.getClosingTrade());
				}
			}
			strategyRepository.deleteById(toKill.getId());
		}
	}
}
