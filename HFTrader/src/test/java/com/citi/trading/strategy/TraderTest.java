package com.citi.trading.strategy;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;
import com.citi.trading.Trade.Result;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricingSource;

/**
 * Unit test of the {@link Trader} base class. We supply a test subclass
 * in order to have a concrete object on which to test the base-class
 * functionality. Traders are heavily wired: we supply mock pricing and
 * market components and a mock persistence layer, and aggressively verify
 * the trader's outbound calls and overall state management.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TraderTest.Config.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class TraderTest {

	public static final String STOCK = "AA";
	public static final int SIZE = 100;
	public static final double PRICE = 100.0;

	public static TwoMovingAverages twoMA = 
			new TwoMovingAverages(STOCK, SIZE, 30000, 60000, 0.03);
	public static TwoMovingAverages twoMAInactive = 
			new TwoMovingAverages(STOCK, SIZE, 30000, 60000, 0.03);
	
	public static class TestableTrader extends Trader<TwoMovingAverages> {

		public static boolean calledHandleWhenOpen;
		public static boolean calledHandleWhenClosed;
		
	    public TestableTrader(PricingSource pricing, OrderPlacer market, 
	    		StrategyPersistence strategyPersistence) {
	    	super(pricing, market, strategyPersistence);
	    }

	    public int getNumberOfPeriodsToWatch() {
			return 12;
		}

		protected void handleDataWhenOpen(PriceData data) {
			calledHandleWhenOpen = true;
		}

		protected void handleDataWhenClosed(PriceData data) {
			calledHandleWhenClosed = true;
		}
		
		public void openPosition() {
			opener.placeOrder(true, 100);
		}
		
		public void confirmOpen(Trade.Result result) {
			Trade trade = new Trade(STOCK, true, SIZE, PRICE);
			trade.setResult(result);
			opener.accept(trade);
		}
		
		public void closePosition() {
			closer.placeOrder(PRICE + 1);
		}
		public void confirmClose(Trade.Result result, int size) {
			Trade trade = new Trade(STOCK, false, size, PRICE + 1);
			trade.setResult(result);
			closer.accept(trade);
		}
	};
	
	@Configuration
	public static class TraderMocks {
		
		@Bean
		public StrategyRepository mockStrategyRepository() {
			return mock(StrategyRepository.class);
		}
		
		@Bean
		public PricingSource mockPricing() {
			return mock(PricingSource.class);
		}
		
		@Bean
		public OrderPlacer mockMarket() {
			return mock(OrderPlacer.class);
		}
		
		@Bean
		public StrategyPersistence mockPersistence() {
			twoMAInactive.setActive(false);
			twoMAInactive.setStopping(false);
			StrategyPersistence mock = mock(StrategyPersistence.class);
			return mock;
		}
	}
	
	@Configuration
	@Import(TraderMocks.class)
	public static class Config {
		
		@Bean
		public TestableTrader trader(PricingSource pricing, OrderPlacer market, 
				StrategyPersistence strategyPersistence) {
			return new TestableTrader(pricing, market, strategyPersistence); 
		}
	}
	
	@Autowired
	private StrategyRepository strategyRepository;
	
	@Autowired
	private TestableTrader trader;
	
	@Autowired
	private PricingSource mockPricing;
	
	@Autowired
	private OrderPlacer mockMarket;
	
	@Autowired
	private StrategyPersistence mockPersistence;
	
	@Before
	public void setUp() {
		TestableTrader.calledHandleWhenOpen = false;
		TestableTrader.calledHandleWhenClosed = false;
		
		twoMA.getPositions().clear();
		trader.setStrategy(twoMA);
	}

	private void openStrategy() {
		Position position = new Position(trader.getStrategy(),
				new Trade(STOCK, true, SIZE, PRICE));
		trader.getStrategy().addPosition(position);
	}

	@SuppressWarnings("unused")
	private void closeStrategy() {
		trader.getStrategy().getPositions().clear();
	}

	@Test(expected=IllegalStateException.class)
	@DirtiesContext
	public void testStartTradingTooSoon() {
		trader.setStrategy(null);
		trader.startTrading(mock(ActiveTraders.class));
		verify(mockPricing, never()).subscribe("AA", 12, trader);
	}

	@Test
	@DirtiesContext
	public void testStartTrading() {
		trader.startTrading(mock(ActiveTraders.class));
		verify(mockPricing).subscribe("AA", 12, trader);
	}

	@Test
	@DirtiesContext
	public void testStopTrading() {
		trader.stopTrading();
		assertThat(trader.getStrategy().isActive(), equalTo(false));
		verify(mockPricing).unsubscribe("AA", trader);
	}
	
	@Test
	public void testAcceptWhenOpen() {
		openStrategy();
		trader.accept(null);
		assertThat(TestableTrader.calledHandleWhenOpen, equalTo(true));
	}
	
	@Test
	public void testAcceptWhenOpenOnPartiallyFilled() {
		trader.confirmOpen(Result.PARTIALLY_FILLED);
		verify(mockPersistence).open(any(), any());
	}
	
	@Test
	public void testAcceptWhenOpenOnRejected() {
		trader.confirmOpen(Result.REJECTED);
		verify(mockPersistence, never()).open(any(), any());
	}
	
	@Test
	public void testAcceptWhenClosed() {
		trader.accept(null);
		assertThat(TestableTrader.calledHandleWhenClosed, equalTo(true));
	}
	
	@Test
	public void testAcceptWhenClosedOnPartiallyFilled() {
		trader.confirmClose(Result.PARTIALLY_FILLED, SIZE);
		verify(mockPersistence).splitAndClosePart(any(), any());
	}
	
	@Test
	public void testAcceptWhenClosedOnRejected() {
		trader.confirmClose(Result.REJECTED, SIZE);
		verify(mockPersistence, never()).close(any(), any());
		verify(mockPersistence, never()).splitAndClosePart(any(), any());
	}
	
	@Test
	@DirtiesContext
	public void testOpening() {
		trader.openPosition();
		verify(mockMarket).placeOrder(any(), any());

		trader.confirmOpen(Trade.Result.FILLED);
		verify(mockPersistence).open(any(), any());
	}

	@Test
	@DirtiesContext
	public void testClosing() {
		openStrategy();
		trader.closePosition();
		verify(mockMarket).placeOrder(any(), any());
		
		trader.confirmClose(Trade.Result.FILLED, SIZE);
		verify(mockPersistence).close(any(), any());
		verify(mockPersistence, never()).splitAndClosePart(any(), any());
	}
	
	@Test
	@DirtiesContext
	public void testStatusChange() {
		Trade trade = new Trade(STOCK, true, SIZE, PRICE);
		trade.setResult(Result.FILLED);
		
		assertThat(trader.getStatus(), equalTo(Trader.Status.STOPPED));
//		trader.startTrading(mock(ActiveTraders.class));
//		assertThat(trader.getStatus(), equalTo(Trader.Status.HUNTING));//from HUNTING
//		trader.accept(null);
//		assertThat(trader.getStatus(), equalTo(Trader.Status.HUNTING));//from HUNTING
//		trader.opener.placeOrder(true, PRICE);
//		assertThat(trader.getStatus(), equalTo(Trader.Status.WAITING));//from WAITING
//		trader.opener.accept(trade);
//		assertThat(trader.getStatus(), equalTo(Trader.Status.HUNTING));
	}
	
	@Test
	@DirtiesContext
	public void testReadyForStop() {
		Trade trade = new Trade(STOCK, true, SIZE, PRICE);
		trade.setResult(Result.FILLED);
		
		assertThat(trader.readyForStop(), equalTo(true));
		trader.startTrading(mock(ActiveTraders.class));
		assertThat(trader.readyForStop(), equalTo(true));
		openStrategy();
		assertThat(trader.readyForStop(), equalTo(false));
		closeStrategy();
		assertThat(trader.readyForStop(), equalTo(true));
		trader.opener.placeOrder(true, PRICE);
//		assertThat(trader.readyForStop(), equalTo(false));
//		trader.opener.accept(trade);
//		assertThat(trader.readyForStop(), equalTo(true));	
	}
	
	@Test
	@DirtiesContext
	public void testPrepareSoftStopWhenClosed() {
		
		trader.prepareStop(mock(ActiveTraders.class));
		trader.opener.placeOrder(true, PRICE);
		
		verify(mockMarket, never()).placeOrder(any(), any());
		assertThat(trader.getStatus(), equalTo(Trader.Status.STOPPED));
	}
	
	@Test
	@DirtiesContext
	public void testPrepareSoftStopWhenOpen() {
		openStrategy();
		trader.prepareStop(mock(ActiveTraders.class));
		assertThat(trader.getStatus(), equalTo(Trader.Status.STOPPING));
		trader.opener.placeOrder(true, PRICE);
		verify(mockMarket, never()).placeOrder(any(), any());
		
		trader.closer.placeOrder(PRICE);
		assertThat(trader.getStatus(), equalTo(Trader.Status.STOPPING));
		verify(mockMarket).placeOrder(any(), any());
	}
}
