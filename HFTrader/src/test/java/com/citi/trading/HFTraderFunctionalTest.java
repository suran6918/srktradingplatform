package com.citi.trading;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.Pricing;
import com.citi.trading.strategy.Position;
import com.citi.trading.strategy.PositionRepository;
import com.citi.trading.strategy.Strategy;
import com.citi.trading.strategy.StrategyRepository;
import com.citi.trading.strategy.TwoMovingAverages;

/**
 * Highest-level functional test. Pricing service, mock market, and the
 * {@link HFTrader} application itself must be running. The mock market must
 * be running in its "alwaysfill" mode, in order to assure predictable trading. 
 * 
 * This test takes about 
 * five and a half minutes: it puts a 2MA trader in play for MRK, which in
 * our pricing service is a predictable, sine-wave pricing curve that should
 * trigger two, symmetrical positions over that time. Afterwards we fetch
 * the updated strategy and check that it has those two positions and the expected
 * profitability and ROI; then we hit the database ourselves to check that 
 * the persistent representation has them, too. We then clean up the trades, 
 * positions, and strategy, hoping to leave not trace.
 * 
 * @author Will Provost
 */
//@RunWith(SpringRunner.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=HFTraderFunctionalTest.Config.class)
public class HFTraderFunctionalTest {

	@Configuration
	@EnableAutoConfiguration
	@PropertySource("classpath:dev.properties")
	public static class Config {
		
		@Bean
		public RestTemplate service() {
			return new RestTemplate();
		}

		@Bean
		public Pricing pricing(RestTemplateBuilder builder) {
			return new Pricing(builder);
		}
	}
	
	@Autowired
	private Pricing pricing;

	@Autowired
	private PositionRepository positionRepository;
	
	@Autowired
	private StrategyRepository strategyRepository;
	
	@Autowired
	private TradeRepository tradeRepository;
	
	@Value("${server.port}")
	private String port;
	
	private String baseURL() {
		return "http://localhost:" + (port != null ? port : "8082") + "/traders";
	}
	
	private static double monitorOpen;
	private static double monitorClose;
	
	@Test
	public void testSinceWave() throws Exception {
		pricing.subscribe("MRK", 1, data -> {
			PricePoint pricePoint = data.getData(1).findAny().get();
			monitorOpen = pricePoint.getOpen();
			monitorClose = pricePoint.getClose();
		});
		System.out.println("Queueing up pricing data ...");
		while (monitorOpen != 59.0566 || monitorClose != 59.5518) {
			pricing.getPriceData();
			System.out.format("Open %1.4f close %1.4f%n", monitorOpen, monitorClose);
			Thread.sleep(15000);
		}
		System.out.println("Ready!");

		RestTemplate template = new RestTemplate();
		
		int ID = 0;
		try {
			TwoMovingAverages twoMA = 
					new TwoMovingAverages("MRK", 100, 60000, 180000, 0.03);
			ResponseEntity<TwoMovingAverages> response = template.postForEntity
					(baseURL(), twoMA, TwoMovingAverages.class);
			assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));
			twoMA = response.getBody();
			ID = twoMA.getId();
			assertThat(ID, not(equalTo(0)));
			
			System.out.println("Strategy created; waiting for 4:00 ...");
			for (int i = 0; i < 22; ++i) {
				Thread.sleep(15000);
				System.out.print(".");
			}
			System.out.println();
			System.out.println("Done; checking on positions and trades ...");
			
			response = template.getForEntity
				(baseURL() + "/" + ID, TwoMovingAverages.class);
			assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

			twoMA = response.getBody();
			assertThat(twoMA.getOpenPosition(), nullValue());
			assertThat(twoMA.getPositions(), hasSize(2));
			assertThat(twoMA.getProfitOrLoss(), closeTo(441.48, 0.0001));
			assertThat(twoMA.getROI(), closeTo(0.0353, 0.0001));
			
		} finally {		
			System.out.println("Deactivating strategy.");
			try {
				template.put(baseURL() + "/" + ID + "/active", "false");
				TwoMovingAverages twoMA = (TwoMovingAverages) 
						strategyRepository.findStrategyAndPositions(ID);
				assertThat(twoMA.isActive(), equalTo(false));
				assertThat(twoMA.getOpenPosition(), nullValue());
				assertThat(twoMA.getPositions(), hasSize(2));
				assertThat(twoMA.getProfitOrLoss(), closeTo(441.48, 0.0001));
				assertThat(twoMA.getROI(), closeTo(0.0353, 0.0001));
			} finally {
				Thread.sleep(15000);
				System.out.println("Removing strategy.");
				try {
					Strategy strategy = strategyRepository.findStrategyAndPositions(ID);
					System.out.println("strategy" + strategy);
					for (Position position : strategy.getPositions()) {
						positionRepository.delete(position);
						tradeRepository.delete(position.getOpeningTrade());
						if (position.getClosingTrade() != null) {
							tradeRepository.delete(position.getClosingTrade());
						}
					}
					strategyRepository.deleteById(ID);
				} catch (Exception ex) {
					ex.printStackTrace(); // but don't report out to the test runner
				}
			}
		}
	}
}
