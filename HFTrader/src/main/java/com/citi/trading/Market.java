package com.citi.trading;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PreDestroy;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

/**
 * Client component for our mock stock market.
 * Requires properties to identify JMS queues for ordering and for receiving
 * notifications of completed (or partially filled, or failed) trades.
 * This component  will manage the asynchornicity of interacting with the
 * market: specifically, it will send an order to the market and return
 * to its caller; then it will handle inbound notifications by calling
 * a method that is registered by the original caller at the time the
 * order was placed; and it checks for timeout on each order and converts a
 * failure to receive a notification for a given trade to an implicit
 * rejection of the trade.
 *
 * @author Will Provost
 */
@Component
public class Market implements OrderPlacer, AutoCloseable {

    private static Logger LOGGER = Logger.getLogger (Market.class.getName ());

    public static final String ID_PREFIX = "Order";
    public static final int TIMEOUT_MSEC = 15000;
    public static final int MONITOR_MSEC = 5000;

	@Autowired
	private JmsTemplate jmsTemplate;

	@Value("${com.citi.trading.Market.orderQueue}")
	private String orderQueue;

  //*** THIS LINE IS HERE TO REMIND YOU: you must replace the value for this property in src/main/resources/dev.properties, so that it's unique, before you run the application. When you've made that fix, remove this line. ***
	@Value("${com.citi.trading.Market.notificationQueue}")
	private String notificationQueue;

	private JAXBContext jaxbContext;
	private int nextID = 1;
	private Map<Integer,Consumer<Trade>> callbacks = new HashMap<>();  //<orderID, Trader>
	private Map<Integer,Trade> orders = new HashMap<>();
	private Timer timer;

	/**
	 * Task that will resolve the status of an outstanding order as rejected,
	 * if it has taken longer than the mock market would ever take to reply to us.
	 */
	private class TimeoutMonitor extends TimerTask {

		@Override
		public void run() {
			synchronized(Market.this) {
				long now = System.currentTimeMillis();
				for (int orderID : orders.keySet()) {
					Trade order = orders.get(orderID);
					if (now - order.getWhen().getTime() > TIMEOUT_MSEC) {  //if order already timed out
						Trade notification = new Trade(order.getWhen(), order.getStock(),
								order.isBuy(), order.getSize(), order.getPrice());
						notification.setResult(Trade.Result.REJECTED);

						Consumer<Trade> callback = callbacks.get(orderID);
						if (callback != null) {
							callback.accept(notification);
							callbacks.remove(orderID);
							orders.remove(orderID);
						} else {
							LOGGER.warning("No callback found for order " + orderID);
						}
						LOGGER.info("Order " + orderID + " timed out.");
					}
				}
			}
		}
	}

	/**
	 * Sets up a JAXB context that will be used to translate {@link Trade}s
	 * to and from the XML representation used by the mock market.
	 * Setss up the timer for the TimeoutMonitor.
	 */
	public Market() {
        try {
            jaxbContext = JAXBContext.newInstance (Trade.class);
        } catch (Exception ex) {
            LOGGER.log (Level.SEVERE, "Couldn't create JAXB context!", ex);
        }

        timer = new Timer();
        timer.schedule(new TimeoutMonitor(), TIMEOUT_MSEC + 1000, MONITOR_MSEC);
	}

	/**
	 * Cancels the timer.
	 */
	@PreDestroy
	public void close() {
		timer.cancel();
	}

	/**
	 * Translates the given trade to an XML string, and sends a JMS TextMessage
	 * with that string as its body to the configured JMS queue. Sets a JMS
	 * correlation ID to be used in matching trade notifications when they come
	 * back to us, and registers the provided callback under that ID.
	 *
	 * @param A trade to be executed on the mock market
	 * @param An object that can be called later with the trade confirmation
	 */
	public synchronized void placeOrder(Trade order, Consumer<Trade> callback) {  //trader is the callback
		try ( StringWriter out = new StringWriter(); ) {
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.marshal(order, out);
			String body = out.toString();
			int orderID = nextID++; 
			jmsTemplate.send(orderQueue, session -> {
					TextMessage message = session.createTextMessage(body);
					message.setJMSReplyTo(jmsTemplate.getDestinationResolver()
							.resolveDestinationName(session, notificationQueue, false));
					message.setJMSCorrelationID(ID_PREFIX + orderID);
					return message;
				});
			callbacks.put(orderID, callback);
			orders.put(orderID, order);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Couldn't place order.", ex);
		}
	}

	/**
	 * JMS listener for trade notifications. Translates the message back to a
	 * {@link Trade} object, which will have its {@link Trade#getResult result}
	 * property filled in with the status of the order. Looks up the registered
	 * callback by the JMS correlation ID, calls that object, and removes it
	 * from the callback registry.
	 */
	@JmsListener(destination="${com.citi.trading.Market.notificationQueue}")
	public synchronized void onReply(Message message, String body) {
		try ( StringReader in = new StringReader(body); ) {
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Trade notification = (Trade) unmarshaller.unmarshal(in);

			int orderID = Integer.parseInt
					(message.getJMSCorrelationID().replace(ID_PREFIX, ""));
			Consumer<Trade> callback = callbacks.get(orderID);
			if (callback != null) {
				callback.accept(notification);  //trader receives success notice
				callbacks.remove(orderID);
				orders.remove(orderID);
			} else {  // TODO: change back to warning
				LOGGER.severe("No callback found for order " + orderID);
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Couldn't process trade notification.", ex);
		}
	}
}
