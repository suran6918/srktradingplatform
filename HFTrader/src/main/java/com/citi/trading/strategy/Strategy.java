
package com.citi.trading.strategy;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;



import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


/**
 * Base representation of a trading strategy. All strategies are dedicated to
 * a specific stock and have a maximum investment in shares bought or sold;
 * and all strategies can be active or inactive. All strategies keep a running
 * list of {@link Position}s taken, in chronological order.
 *
 * @author Will Provost
 */

@TableGenerator ( 
  name="strategyGenerator", 
  table="id_sequences", 
  pkColumnName="sequence_name", 
  valueColumnName="sequence_value", 
  pkColumnValue="strategies", 
  allocationSize=1, 
  initialValue=101 // gives us some space for explicit test data
)

@Entity
@Inheritance (strategy=InheritanceType.JOINED)
@DiscriminatorColumn 
	(name = "strategy_type", discriminatorType=DiscriminatorType.CHAR)
@DiscriminatorValue("Z") // makes Hibernate happy

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({@Type(name="2MA", value=TwoMovingAverages.class),@Type(name="BB", value=BollingerBands.class)}) 

public abstract class Strategy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue 
	  (strategy=GenerationType.TABLE, generator="strategyGenerator")
	private int id;

	private boolean active;
	private String stock;
	private int size;
	private Timestamp startedTrading;
	private Timestamp stoppedTrading;
	
	@OneToMany (mappedBy="strategy")
	private List<Position> positions = new ArrayList<>();
	private boolean stopping;
	
   
	public Strategy() {
	}
	
	public Strategy(String stock, int size) {
		this.active = true;
		this.stopping = false;
		this.stock = stock;
		this.size = size;
	}
	
	public boolean getStopping() {
		return stopping;
	}
	
	public void setStopping(boolean isStopping) {
		this.stopping = isStopping;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getStock() {
		return this.stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}
	
	public Timestamp getStartedTrading() {
		return startedTrading;
	}
	
	public void setStartedTrading(Timestamp startedTrading) {
		this.startedTrading = startedTrading;
	}

	public Timestamp getStoppedTrading() {
		return stoppedTrading;
	}
	
	public void setStoppedTrading(Timestamp stoppedTrading) {
		this.stoppedTrading = stoppedTrading;
	}

	public List<Position> getPositions() {
		return this.positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}
	
	public Position getOpenPosition() {
		Optional<Position> found = positions.stream().filter(Position::isOpen).findAny();
		return found.isPresent() ? found.get() : null;
	}

	public Position addPosition(Position position) {
		getPositions().add(position);
		position.setStrategy(this);

		return position;
	}

	public Position removePosition(Position position) {
		getPositions().remove(position);
		position.setStrategy(null);

		return position;
	}
	
	/**
	 * Reports the total profit or loss for all closed positions. 
	 */
	public double getProfitOrLoss() {
		return getPositions().stream().filter(p -> !p.isOpen())
				.mapToDouble(Position::getProfitOrLoss).sum();
	}
	
	/**
	 * Returns the weighted ROI for all closed positions.
	 */
	public double getROI() {
		return getProfitOrLoss() / 
			getPositions().stream().filter(p -> !p.isOpen())
				.mapToDouble(p -> p.getOpeningTrade().getPrice() * p.getOpeningTrade().getSize())
				.sum();
	}
	
	/**
	 * Helper for <strong>toString()</strong> methods as implemented by 
	 * derived classes.
	 */
	protected String stringRepresentation() {
		return String.format("stock=%s, size=%d, active=%s -- %s", 
			stock, size, active, getOpenPosition() != null 
				? "openingTrade=" + getOpenPosition().getOpeningTrade()
				: "closed");
	}
}

