package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;


@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BollingerBandsTrader extends Trader<BollingerBands> {
	
    private static final Logger LOGGER =
        Logger.getLogger(BollingerBandsTrader.class.getName ());
    
    private boolean highBand; // false=buy, true = sell, 
    private boolean placeOrder = false;
    
  public BollingerBandsTrader(PricingSource pricing,OrderPlacer market, StrategyPersistence strategyPersistence) {
	super(pricing, market, strategyPersistence);
  }
	
  protected int periods(int msec) {

//		LOGGER.info("SHEHERYAR (BBands): VALUE FOR PERIODS " + Integer.toString(msec / 1000 / SECONDS_PER_PERIOD) + "msec value: " + msec);
  	return msec / 1000 / SECONDS_PER_PERIOD;
  }
  
  	// could be useless for bollinger bands
	public int getNumberOfPeriodsToWatch() {
		return periods(strategy.getLength());
	}
	
	@Override
	protected void handleDataWhenOpen(PriceData data) {
		//need a way to update if we should buy or not
		buyOrSellOpening(data);
		
		double currentPrice = data.getData(1).findAny().get().getClose();
    	double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
    	double profitOrLoss = currentPrice / openingPrice - 1.0;
    	// close the position once exit threshold met
    	if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
    		closer.placeOrder(currentPrice);
    		//if the trade for opening position was a sell (sold@62.59  bought@60.39 --> current/opening -1 = negative value for profitOrLoss 
    		// but we made a profit therefore 0 - profitOrLoss) 
    		if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
    			profitOrLoss = 0 - profitOrLoss;
    		}
    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
    				profitOrLoss * 100));
    	}
		
	}
	
	/** 
	 * WE ARE LOOKING TO OPEN A NEW POSITON
	 * 
	 * Check if we have enough data first, if so:
	 * 		1) Call buyOrSellOpening() to determine if we want to buy or sell
	 * 		2)
	 * **/
	@Override
	protected void handleDataWhenClosed(PriceData data) {
		if (tracking.get()) {
			buyOrSellOpening(data);
			//ready to place an order (buy or sell)
			if(placeOrder) {
    			opener.placeOrder(!highBand, data.getData(1).findAny().get().getClose());
    			LOGGER.info("Trader " + strategy.getId() + " opening position as average crossed " +
    					(highBand ? "High Band" : "Low Band") + "which means its time to "+(highBand? "sell": "buy"));
			}
		}
		else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
//	    	checkAverages(data);
	    	tracking.set(true);
	    	LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages.");
	    }
		
	}
	
	private void buyOrSellOpening(PriceData data){
		
		double standardDeviation = data.calculateSTD(getNumberOfPeriodsToWatch(), PricePoint::getClose);
		double mean = data.getWindowAverage(getNumberOfPeriodsToWatch(), PricePoint::getClose);
		double currentPrice = data.getData(1).findAny().get().getClose();
		
		//SELL: price is higher then average
		if(currentPrice >= mean + (standardDeviation*strategy.getMultiple())) {
//		    buyStock=false; // true=buy, false = sell, 
			highBand = false; // false=buy, true = sell,
		    placeOrder = true;
		}// BUY: price is below average (by 2 STD's) 
		else if(currentPrice <= mean - (standardDeviation*strategy.getMultiple())) {
//			buyStock=true; // true=buy, false = sell, 
			highBand = true; // false=buy, true = sell,
		    placeOrder = true;
		}else {
			placeOrder=false;
		}
		
		LOGGER.fine(String.format("Trader " + strategy.getId() + " as of %s averages are %1.4f with standard deviation of %1.4f, and current price %1.4f ",
    			data.getLatestTimestamp(), mean, standardDeviation, currentPrice));
		
	}
	
//	private double calculateSTD(PriceData data) {
//		
//		// Step 1) calculate mean 
//		double mean = data.getWindowAverage(getNumberOfPeriodsToWatch(), PricePoint::getClose);
//		
//		// Step 2 & 3) Subtract the mean from each data point and square it
//			//extract all N recent prices 
//		List<Double> prices = new ArrayList<>();
//		data.getData(getNumberOfPeriodsToWatch())
//		.forEach(
//				price -> prices.add(Math.pow((price.getClose()-mean),2))
//				);
//		
//		// Step 4) Calculate the mean of the squared differences
//		double newMean = (prices.stream().mapToDouble(Double::doubleValue).sum())/prices.size();
//		
//		//Step 5) Calculate STD
//		
//		return Math.sqrt(newMean);
//	}

}
