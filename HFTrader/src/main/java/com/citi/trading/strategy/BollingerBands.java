package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Represents a BollingerBands trading strategy, adding the parameters
 * that are specific to that algorithm.
 */
@Entity
@DiscriminatorValue("B")
public class BollingerBands  extends Strategy implements Serializable{
	private static final long serialVersionUID = 1L;

	private int length;
	private double multiple;
	private double exitThreshold;

	public BollingerBands() {
	}
	
	public BollingerBands(String stock, int size, int length, double exitThreshold) {
		super(stock, size);
		this.length=length;
		this.exitThreshold = exitThreshold;
	}

	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}


	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	public double getMultiple() {
		return multiple;
	}

	public void setMultiple(double multiple) {
		this.multiple = multiple;
	}

	@Override
	public String toString() {
		return "BollingerBands [length=" + length + ", exitThreshold=" + exitThreshold + "]";
	}

}
