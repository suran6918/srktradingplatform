package com.citi.trading.strategy;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.citi.trading.Trade;
import com.citi.trading.TradeRepository;

/**
 * Persistence service implementation using Spring Data repositories.
 * 
 * @author Will Provost
 */
@Component
public class StrategyPersistenceImpl implements StrategyPersistence {

	@Autowired
	public TradeRepository tradeRepository;
	
	@Autowired
	public StrategyRepository strategyRepository;
	
	@Autowired
	public PositionRepository positionRepository;
	
	/**
	 * Create a new position for the given strategy, with the given opening trade.
	 */
	@Transactional
	public Position open(Strategy strategy, Trade openingTrade) {
		
		if (strategy.getOpenPosition() != null) {
			throw new IllegalStateException
				("Strategy " + strategy.getId() + " already has an open position.");
		}

		openingTrade = tradeRepository.save(openingTrade);
		
		Position position = new Position(strategy, openingTrade);
		position = positionRepository.save(position);
		
		strategy.addPosition(position);
		
		return position;
	}

	/**
	 * Close the currently open position, with the given closing trade.
	 */
	@Transactional
	public Position close(Strategy strategy, Trade closingTrade) {
		
		Position position = strategy.getOpenPosition();
		if (position == null) {
			throw new IllegalStateException
				("Strategy " + strategy.getId() + " has no open position that could be closed.");
		}
		
		Trade openingTrade = position.getOpeningTrade();
		if (!openingTrade.getStock().equals(closingTrade.getStock()) ||
				openingTrade.isBuy() == closingTrade.isBuy() ||
				openingTrade.getSize() != closingTrade.getSize()) {
			throw new IllegalArgumentException
				(String.format("For strategy " + strategy.getId() + ", the closing trade is incompatible with the opening trade: (%s, %s, %d) vs. ((%s, %s, %d).",
					openingTrade.getStock(), "" + openingTrade.isBuy(), openingTrade.getSize(),
					closingTrade.getStock(), "" + closingTrade.isBuy(), closingTrade.getSize()));
		}
		
		closingTrade = tradeRepository.save(closingTrade);

		position.setClosingTrade(closingTrade);
		return positionRepository.save(position);
	}

	/**
	 * Split the currently open position, in case of a partial fill. 
	 * This means (a) reduce the size of the opening trade to match the share 
	 * count of the closing trade, (b) record the closing trade, and
	 * (c) open a new position representing the unfilled balance, so that
	 * we will continue to look to close the rest of the position. 
	 */
	@Transactional
	public Position splitAndClosePart(Strategy strategy, Trade closingTrade) {
		
		Position position = strategy.getOpenPosition();
		if (position == null) {
			throw new IllegalStateException
				("Strategy " + strategy.getId() + " has no open position that could be closed.");
		}
		
		Trade openingTrade = position.getOpeningTrade();
		if (!openingTrade.getStock().equals(closingTrade.getStock()) ||
				openingTrade.isBuy() == closingTrade.isBuy() ||
				openingTrade.getSize() <= closingTrade.getSize() ||
				closingTrade.getSize() == 0) {
			throw new IllegalArgumentException
				(String.format("For strategy \" + strategy.getId() + \", the closing trade is incompatible with the opening trade: (%s, %s, %d) vs. ((%s, %s, %d).",
					openingTrade.getStock(), "" + openingTrade.isBuy(), openingTrade.getSize(),
					closingTrade.getStock(), "" + closingTrade.isBuy(), closingTrade.getSize()));
		}
		
		int leftOpen = openingTrade.getSize() - closingTrade.getSize();
		openingTrade.setSize(closingTrade.getSize());
		openingTrade = tradeRepository.save(openingTrade);
		position.setOpeningTrade(openingTrade);
		
		closingTrade = tradeRepository.save(closingTrade);
		
		position.setClosingTrade(closingTrade);
		position = positionRepository.save(position);
		
		Trade newOpeningTrade = new Trade(openingTrade.getWhen(), openingTrade.getStock(), 
				openingTrade.isBuy(), leftOpen, openingTrade.getPrice());
		return open(strategy, newOpeningTrade);
	}

	/**
	 * Set the trading start time for the strategy to current system time, 
	 * and wipe any previous stop time to null.  
	 */
	public Strategy startTrading(Strategy strategy) {
		strategy = strategyRepository.findById(strategy.getId()).get();
		strategy.setStartedTrading(new Timestamp(System.currentTimeMillis()));
		strategy.setStoppedTrading(null);
		strategyRepository.save(strategy);
		return strategyRepository.findStrategyAndPositions(strategy.getId());
	}
	
	/**
	 * Set the trading stop time for the strategy to current system time.  
	 */
	public Strategy stopTrading(Strategy strategy) {
		strategy = strategyRepository.findById(strategy.getId()).get();
		strategy.setStoppedTrading(new Timestamp(System.currentTimeMillis()));
		strategyRepository.save(strategy);
		return strategyRepository.findStrategyAndPositions(strategy.getId());
	}
}
