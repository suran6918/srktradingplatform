
package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.logging.Logger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

/**
 * Implementation of the two-moving-averages trading strategy.
 * We check the long and short rolling average of the closing prices,
 * and track whether one or the other is greater. When they cross, we
 * open a position. Once open, we close on a configurable percentage
 * gain or loss.
 *
 * @author Will Provost
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TwoMovingAveragesTrader extends Trader<TwoMovingAverages> {
	
    private static final Logger LOGGER =
        Logger.getLogger(TwoMovingAveragesTrader.class.getName ());
    
    private boolean shortIsEqual = true;
    private boolean shortIsGreater;
    
    public TwoMovingAveragesTrader(PricingSource pricing, 
    		OrderPlacer market, StrategyPersistence strategyPersistence) {
    	super(pricing, market, strategyPersistence);
    }
    
    protected int periods(int msec) {

// 		LOGGER.info("SHEHERYAR: VALUE FOR PERIODS " + Integer.toString(msec / 1000 / SECONDS_PER_PERIOD) + "msec value: " + msec);
    	return msec / 1000 / SECONDS_PER_PERIOD;
    }
    
    public int getNumberOfPeriodsToWatch() {
    	return periods(strategy.getLengthLong());
    }
    
    /**
     * Helper method to update our flags: which average is greater, or are they
     * exactly or effectively equal?
     */
    private void checkAverages(PriceData data) {
        final double TOLERANCE = 0.0001;

        double shortAverage = data.getWindowAverage
    			(periods(strategy.getLengthShort()), PricePoint::getClose);
    	double longAverage = data.getWindowAverage
    			(periods(strategy.getLengthLong()), PricePoint::getClose);
    	
    	shortIsEqual = Math.abs(shortAverage - longAverage) / shortAverage < TOLERANCE;
    	shortIsGreater = shortAverage > longAverage;
    	
    	LOGGER.fine(String.format("Trader " + strategy.getId() + " as of %s averages are %1.4f %s %1.4f",
    			data.getLatestTimestamp(), shortAverage, 
    			shortIsEqual ? "=" : (shortIsGreater ? ">" : "<"), longAverage));
    }
    
    /**
     * When we're open, just check to see if the latest closing price is a profit or
     * loss greater than our configured threshold. If it is, use the base class'
     * <strong>Closer</strong> to close the position.
     */
    protected void handleDataWhenOpen(PriceData data) { 
    	checkAverages(data);
    	double currentPrice = data.getData(1).findAny().get().getClose();
    	double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
    	double profitOrLoss = currentPrice / openingPrice - 1.0;
    	if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
    		closer.placeOrder(currentPrice);
    		if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
    			profitOrLoss = 0 - profitOrLoss;
    		}
    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
    				profitOrLoss * 100));
    	}
    }
    
    /**
     * When we're closed, capture the most recent greater-than/equal flags,
     * update to get the new ones, and see if there's been a change.
     * If short was lower and is now higher, buy to open a long position;
     * if short was higher and is now lower, sell to open a short position.
     */
    protected void handleDataWhenClosed(PriceData data) {
    	if (tracking.get()) {
    		boolean shortWasEqual = shortIsEqual;
    		boolean shortWasGreater = shortIsGreater;
    		checkAverages(data);
    		
    		if (shortWasGreater != shortIsGreater && !shortWasEqual && !shortIsEqual) {
    			opener.placeOrder(shortIsGreater, data.getData(1).findAny().get().getClose());
    			LOGGER.info("Trader " + strategy.getId() + " opening position as short average crossed " +
    					(shortIsGreater ? "over" : "under") + " long average.");
    		}
	    } else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
	    	checkAverages(data);
	    	tracking.set(true);
	    	LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages.");
	    }
    }
}
