package com.citi.trading.strategy;

import java.util.Observable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricingSource;


/**
 * Base implementation for trading logic. Any trader has a {@link Strategy}
 * that provides the parameters for the specific type of trader --
 * e.g. a two-moving-averages trader implements the logic of the 2MA 
 * algorithm, actively reading pricing data, placing trader orders, etc.,
 * and knows its stock, size, short and long average lengths, and exit 
 * threshold by consulting the associated two-moving-averages strategy object.
 * Strategies are persistent and stateful; traders are transient and
 * behavioral, consulting the strategies for persistent state and, it's true,
 * holding some transient state of their own.
 * 
 * This base class knows how to interact with the pricing service and the
 * mock market, and how to record positions and trades when they happen.
 * It does not know how to decide when to buy or sell, and it looks to 
 * derived classes to do that.
 * 
 * @author Will Provost
 */
public abstract class Trader<S extends Strategy> extends Observable implements Consumer<PriceData>
{
    private static final Logger LOGGER = Logger.getLogger(Trader.class.getName ());

    protected S strategy;
    protected AtomicBoolean tracking = new AtomicBoolean(false);
    
    protected PricingSource pricing;
    protected OrderPlacer market;
    protected StrategyPersistence strategyPersistence;
    @Autowired
    protected StrategyRepository strategyRepository;
    
    public enum Status{
    	HUNTING,  //hunting but no order placed, can have open positions
    	WAITING,   //waiting for order reply -- waiting to open or close a position
    	STOPPING,  //after user requested a stop, can have open position
    	STOPPED    //all positions closed -- trader stopped or are valid to stop
    }
    protected Status status = Status.STOPPED;


    /**
     * Provide the three big externalities -- pricing feed, mock market, and
     * persistence provider -- as constructor arguments in an ordinary Java 
     * <strong>new</strong> call, or when created as a Spring bean these will
     * all be injected to the constructor. 
     */
    public Trader(PricingSource pricing, OrderPlacer market, 
    		StrategyPersistence strategyPersistence) {
    	this.pricing = pricing;
    	this.market = market;
    	this.strategyPersistence = strategyPersistence;
    }    
    
    public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public S getStrategy() {
    	return strategy;
    }
    
    public void setStrategy(S strategy) {
    	this.strategy = strategy;
    }
    
    /**
     * Since the strategy can't be dependency-injected (it's brought up
     * from the database during the run of the application), this helper 
     * method enforces a rule that the strategy must be set before 
     * starting any operations.
     * 
     * @throws IllegalStateException If the strategy is null
     */
    protected void checkStrategy() {
    	if (strategy == null) {
    		throw new IllegalStateException
    			("You must set a strategy object, so I know what to do.");
    	}
    }
    
    public boolean isTracking() {
    	return tracking.get();
    }
    
    /**
     * @return True if the strategy has an open position.
     */
    public boolean isOpen() {
    	checkStrategy();
    	return strategy.getOpenPosition() != null;
    }
    
    
    /**
     * Derived classes inform us as to how many periods of pricing data  
     * they will need to check as part of their decision-making.
     */
    public abstract int getNumberOfPeriodsToWatch();
    
    /**
     * Derived classes implement this to make their trading decisions;
     * called whenever the trader has closed its most recent position and is
     * on the hunt for a new one.
     */
    protected abstract void handleDataWhenOpen(PriceData data);

    /**
     * Derived classes implement this to make their trading decisions;
     * called whenever the trader has an open position.
     */
    protected abstract void handleDataWhenClosed(PriceData data);
    
    /**
     * If the current strategy is stopping, continue stopping. Otherwise,
     * set trader status to hunting to go back to trading.
     * Subscribe to the pricing service.
     */
	public synchronized void startTrading(ActiveTraders activeTraders) {
    	checkStrategy();
    	if(strategy.getStopping()) {
       		this.prepareStop(activeTraders);
        	LOGGER.info("Trader " + strategy.getId() + " has started trading but will soon close because it was trying to stop before!"); 	
    	}else {
	   		status = Status.HUNTING;
	    	LOGGER.info("Trader " + strategy.getId() + " has started trading.");
    	}
    	
    	pricing.subscribe(strategy.getStock(), getNumberOfPeriodsToWatch(), this);
    	tracking.set(false);
   		strategy.setActive(true);
   		strategyRepository.save(strategy);
	}
    
    /**
     * Unsubscribe from the pricing service.
     */
	public synchronized void stopTrading() {
    	checkStrategy();
    	pricing.unsubscribe(strategy.getStock(), this);
   		strategy.setActive(false);
   		strategyRepository.save(strategy);
    	LOGGER.info("Trader " + strategy.getId() + " has stopped trading.");
    }
    
    /**
     * Pass the pricing notification to one of the two handler methods
     * implemented by the derived class.
     */
    public synchronized void accept(PriceData data) {
    	if (isOpen()) {
   			handleDataWhenOpen(data);
    	} else {
    		handleDataWhenClosed(data);
    	}
    }
 
    /**
     * Prepare the current trader to get ready for a stop. Add activeTraders
     * as observer to monitor the trading status.
     */
    public synchronized void prepareStop(ActiveTraders activeTraders) {
    	this.status = Status.STOPPING;
    	strategy.setStopping(true);
    	strategyRepository.save(strategy);
    	this.addObserver(activeTraders);
    }
    

    /**
     * Check whether a trader is ready for stop for being neither waiting for
     * a order reply nor is in open position. 
     */
    public boolean readyForStop() {
    	return !this.status.equals(Status.WAITING) && !this.isOpen();
    }
    
    /**
     * Trader notifies ActiveTraders that it is now ready to be removed.
     */
    public synchronized void stop() {
    	LOGGER.info("Trader stopping, notifying ActiveTraders");
    	this.status = Status.STOPPED;
    	this.setChanged();
		this.notifyObservers();
	}
    
    /**
     * Derived classes use the instance of this class to make trade orders.
     */
    protected class Opener implements Consumer<Trade> {
    	
    	/**
    	 * Creates a {@link Trade} representing our offer to buy or sell stock,
    	 * synthesizing the strategy's stock and size with the given buy/sell flag
    	 * and offer price. Sends this to the mock market, with ourselves
    	 * (the {@link Trader#opener} instance) as the callback.
    	 * Updates the trading status of the trader.
    	 */
    	public void placeOrder(boolean buy, double price) {
    		if(status.equals(Status.STOPPING)) {
    			if(readyForStop()) {
    				stop();
    			}
    			return;
    		}
    		
    		checkStrategy();
    		if (isOpen()) {
    			throw new IllegalStateException("We already have an open position.");
    		}
    		
    		Trade trade = new Trade
    				(strategy.getStock(), buy, strategy.getSize(), price);
    		market.placeOrder(trade, this);  
    		status = Status.WAITING;
    	}
    	

		/**
    	 * Checks the results field of the notification. If the trade was fully filled,
    	 * record as a new, open position. If partially filled, record that as a smaller
    	 * open position and live with the fact that we're investing less than our maximum.
    	 * If rejected, don't do anything: we're back on the hunt for an opening signal. 
    	 * Updates the trading status of the trader.
    	 */
    	public void accept(Trade notification) { 
   			if(notification.getResult().equals(Trade.Result.REJECTED)) {
   				LOGGER.info("Trader " + strategy.getId() + " open trade rejected: " + notification);
   			} else {
   				LOGGER.info("Trader " + strategy.getId() + " has opened: " + notification);
   				strategyPersistence.open(strategy, notification);
   			}
   			if(status.equals(Status.STOPPING) && readyForStop()) {
   				stop();
   				return;
   			}
   			if(!status.equals(Status.STOPPING))
   				status = Status.HUNTING;
    	}
    }
    
    protected Opener opener = new Opener();
    
    /**
     * Derived classes use the instance of this class to make trade orders.
     */
    protected class Closer implements Consumer<Trade> {
    	
    	/**
    	 * Creates a {@link Trade} representing our offer to buy or sell stock,
    	 * symmetrical to our opening trade but with the given offer price.
    	 * Sends this to the mock market, with ourselves
    	 * (the {@link Trader#closer} instance) as the callback.
    	 * Updates the trading status of the trader.
    	 */
    	public void placeOrder(double price) {
    		if(status.equals(Status.STOPPING) && readyForStop()) {
    			stop();
    			return;
    		}
    		
    		checkStrategy();
    		Position position = strategy.getOpenPosition();
    		if (strategy.getOpenPosition() == null) {
    			throw new IllegalStateException("We don't have an open position to close.");
    		}

    		Trade trade = new Trade(strategy.getStock(), 
    				!position.getOpeningTrade().isBuy(), 
    				position.getOpeningTrade().getSize(), price);
    		market.placeOrder(trade, this);
    		if(!status.equals(Status.STOPPING)) {
    			status = Status.WAITING;

    		}
    	}
    	
    	/**
    	 * Checks the results field of the notification. If the trade was fully filled,
    	 * close the current position. If partially filled, split the current position.
    	 * If rejected, don't do anything: we'll continue to look for signals to close,
    	 * but for example if the price falls back inside our threshold we may wait
    	 * a while longer. Updates the trading status of the trader.
    	 */
    	public void accept(Trade notification) {
    		Trade.Result result = notification.getResult();
    		switch(result) {
	    		case PARTIALLY_FILLED:
	    			LOGGER.info("Trader " + strategy.getId() + " has closed: " + notification + " and opened a "
	    						+ "open position for the remaining balance.");
	    			strategyPersistence.splitAndClosePart(strategy, notification);
	    			break;
	    		case FILLED:
	    			LOGGER.info("Trader " + strategy.getId() + " has closed: " + notification);
	   				strategyPersistence.close(strategy, notification);
	   				break;
	   			default:
	   				LOGGER.info("Trader " + strategy.getId() + " failed to close the trade: " + notification);
	   				break;
    		}  
    		if(status.equals(Status.STOPPING)) {
    			if(readyForStop()) {
    				stop();
       				return;
    			}
    		} else {
    			status = Status.HUNTING;
    		}
    	}
    }
    
    protected Closer closer = new Closer();
    
    @Override
    public String toString() {
    	return String.format("Trader<%s>", strategy);
    }

}

