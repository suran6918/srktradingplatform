package com.citi.pricing;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.function.Function;

public class TrackedStock
{
    private static final Random generator = new SecureRandom(); 
    private static final String HEADER = "timestamp,open,high,low,close,volume\n";
    private static final int SECONDS_IN_PERIOD = 15;
    private static final int PERIODS = 120;
    private static final int POINTS_PER_PERIOD = 10;
    
    private double basePrice;
    private Wave[] waves;
    private double fuzz;
    private Surge[] surges;
    
    private LocalDateTime latestTime;
    private String[] CSV;
    private int startingPoint;
    private double latestPrice;
    
    public TrackedStock(double basePrice, Wave[] waves, double fuzz, Surge[] surges) {
        this.basePrice = basePrice;
        this.waves = waves;
        this.fuzz = fuzz;
        this.surges = surges;
        
        this.latestTime = LocalDateTime.now().minusSeconds(PERIODS * SECONDS_IN_PERIOD);
        this.latestTime = this.latestTime.minusSeconds(this.latestTime.getSecond() % 15);
        
        this.CSV = new String[PERIODS];
        this.startingPoint = 0;
        this.latestPrice = basePrice;
    }

    public double getNextPrice() {
        double price = basePrice;
        for (Wave wave : waves) {
            price += wave.getNextImpact() * basePrice;
        }
        for (Surge surge : surges) {
            price += surge.getNextImpact() * basePrice;
        }
        price += (generator.nextDouble () * 2 - 1) * fuzz * price;  
            
        return price;
    }
    
    public String getLatestData(int periods) {
        
        int periodsToUpdate = (int) Duration.between(latestTime, LocalDateTime.now())
            .getSeconds () / SECONDS_IN_PERIOD;

        for (int period = 0; period < periodsToUpdate; ++period) {
            double open = 0;
            double high = 0;
            double low = basePrice * 10;
            for (int point = 0; point < POINTS_PER_PERIOD; ++point) {
                latestPrice = getNextPrice();
                if (open == 0) {
                    open = latestPrice;
                }
                if (high < latestPrice) {
                    high = latestPrice;
                }
                if (low > latestPrice) {
                    low = latestPrice;
                }
            }
            
            CSV[startingPoint] = String.format("%s %s,%1.4f,%1.4f,%1.4f,%1.4f,%d", 
                latestTime.format (DateTimeFormatter.ISO_LOCAL_DATE), 
                latestTime.format (DateTimeFormatter.ISO_LOCAL_TIME), 
                open, high, low, latestPrice, 
                (int) (generator.nextDouble() * 97000) + 3000);
            
            latestTime = latestTime.plusSeconds(SECONDS_IN_PERIOD);
            startingPoint = (startingPoint + 1) % PERIODS;
        }
        
        StringBuilder result = new StringBuilder(HEADER);
        if (periods > PERIODS) {
            periods = PERIODS;
        }
        for (int offset = PERIODS - periods; offset < PERIODS; ++offset) {
            result.append(CSV[(startingPoint + offset) % PERIODS]).append("\n"); 
        }
        return result.toString();
    }
    
    // Generally volatile enough for high-frequency algorithms to trade on
    public static final Function<Double,TrackedStock> PROFILE1 = basePrice -> {
        Wave[] waves = { new Wave(.01, 50, 4, 0), new Wave(.02, 200, 0, 0) };
        Surge[] surges = { new Surge(.012, 4), new Surge(.01, 6) };
        return new TrackedStock(basePrice, waves, .005, surges);
    };

    // Like #1 but less volatile short-term, more prone to gradual moves into new territory
    public static final Function<Double,TrackedStock> PROFILE2 = basePrice -> {
        Wave[] waves = { new Wave(.01, 50, 4, 0), new Wave(.02, 200, 0, 0) };
        Surge[] surges = { new Surge(.002, 10), new Surge(.01, 6) };
        return new TrackedStock(basePrice, waves, .005, surges);
    };
    
    // Totally predictable, one perfect sine wave over 200 price points
    public static final Function<Double,TrackedStock> PROFILE3 = basePrice -> {
        Wave[] waves = { new Wave(.06, 200, 0, 0) };
        Surge[] surges = {};
        return new TrackedStock(basePrice, waves, 0.0, surges);
    };

    // Totally predictable, two sine waves at 200 and 1000 price points
    public static final Function<Double,TrackedStock> PROFILE4 = basePrice -> {
        Wave[] waves = { new Wave(.05, 200, 0, 0), new Wave(.03, 1000, 0, 0) };
        Surge[] surges = {};
        return new TrackedStock(basePrice, waves, 0.0, surges);
    };

    // Two sine waves at 200 and 1000 price points dominate,
    // and then some fuzziness around that for looks.
    public static final Function<Double,TrackedStock> PROFILE5 = basePrice -> {
        Wave[] waves = { new Wave(.05, 200, 0, 0), new Wave(.03, 1000, 0, 0) };
        Surge[] surges = { new Surge(.01, 10) };
        return new TrackedStock(basePrice, waves, 0.0, surges);
    };

    public static void main(String[] args) throws Exception {
        TrackedStock stock = PROFILE5.apply(20.0);
        try ( PrintWriter out = new PrintWriter(new FileWriter("Prices.csv")); ) {
            out.println(stock.getLatestData (100));
        }
        try { Thread.sleep(60000); } catch(Exception ex) {}
        try ( PrintWriter out = new PrintWriter(new FileWriter("Prices1MinLater.csv")); ) {
            out.println(stock.getLatestData (8));
        }
    }
}
