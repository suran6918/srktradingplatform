package com.citi.pricing;

import java.security.SecureRandom;
import java.util.Random;

public class Surge
{
    private static final Random generator = new SecureRandom();
    
    private double momentum;
    private double amplitude;
    private int threshold;
    
    public Surge(double amplitude, int threshold) {
        this.amplitude = amplitude;
        this.threshold = threshold;
        this.momentum = 0;
    }

    public double getNextImpact() {
        momentum += (generator.nextDouble () - .5) * 2 * amplitude;
        if (momentum > amplitude * threshold) {
            momentum = -amplitude;
        }
        if (momentum < -amplitude * threshold) {
            momentum = amplitude;
        }
        
        return momentum;
    }

    
    public static void main(String[] args) {
        Surge surge = new Surge(.05, 5);
        for (int x = 0; x < 50; ++x) {
            System.out.println(surge.getNextImpact ());
        }
    }
}
