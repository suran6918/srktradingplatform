import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TraderTable } from '../view/trader-table';
import { TraderToolbar } from '../view/trader-toolbar';
import { PopUpComponent } from '../view/pop-up/pop-up.component';
import { Modal } from 'src/view/modal';
import { NavbarComponent } from '../view/navbar/navbar.component';
import { InputPopupComponent } from '../view/input-popup/input-popup.component';
import { MatSortModule } from '@angular/material';
import { FilterComponent } from '../view/filter/filter.component';
import { MainComponent } from './main/main.component';


import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AnalysisComponent } from './analysis/analysis.component';

@NgModule({
  declarations: [
    AppComponent,
    TraderTable,
    TraderToolbar,
    PopUpComponent,
    Modal,
    NavbarComponent,
    InputPopupComponent,
    FilterComponent,
    MainComponent,
    AnalysisComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    MatSortModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
