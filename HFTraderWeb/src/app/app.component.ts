import { TraderTable } from "../view/trader-table";
import { DatashareService } from 'src/view/datashare.service';
import {Component, Injectable} from '@angular/core';

/**
 * The root Angular component that holds all child components.
 */
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "SRK Trading Platform";
}
