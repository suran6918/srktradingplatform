import { Component, OnInit } from '@angular/core';
import { DatashareService } from 'src/view/datashare.service';

/**
 * The main page Angular component that the default path maps to,
 * holds all child components of the main page, intermediates 
 * interactions between child components.
 */
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  TmaChecked: boolean = true;
  BbChecked: boolean = true;
  public filterEvent: Event;
  public stocksEvent: Event;
  constructor(private dataService: DatashareService) { }

  id :string
  newTrader($event){
    this.id = $event
    this.dataService.onGetInputGroup(this.id)
    console.log('creating new trader' + this.id)
  }
  ngOnInit() {
  }

  /**
   * Update the checkBox status based on user input. 
   */
  checkBox(type){
    switch(type){
      case "2MA":
        this.TmaChecked? this.TmaChecked=false:this.TmaChecked=true;
        break;
      case "BB":
          this.BbChecked? this.BbChecked=false:this.BbChecked=true;
    }
    
  }

  /**
   * filterApplied event listener. 
   */
  onFilterApplied(event: Event){
    this.filterEvent = event;
  }

  /**
   * stocksChange event listener.
   */
  onStocksChange(event: Event){
    this.stocksEvent = event;
  }

  /**
   * strategyCheck event listener. 
   */
  onStrategyCheck(name){
    this.checkBox(name);
  }
}
