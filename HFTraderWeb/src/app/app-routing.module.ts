import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AnalysisComponent } from './analysis/analysis.component';



const routes: Routes = [
  {path:'',component:MainComponent},
  {path:'analysis', component:AnalysisComponent}
// {path:'about', component:AboutComponent},
// {path:'info', redirectTo:"/about", pathMatch:'full'},
// {path:'clock', component:ClockComponent, data:{title:'Local Time Snapshot'}},
// {path:'stock', component:StockParamsComponent},
// {path:'stock/:id', component:StockParamsComponent},
// stock VS stock/:id/:symbol basically says /:id/:symbol has a parameter named "id" and "symbol"
// {path:'**',component:PageNotFoundComponent}

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
