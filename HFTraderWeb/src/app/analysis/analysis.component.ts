import { Component, OnInit } from '@angular/core';
import { stockPrices } from 'src/model/stockPrices';
import { TraderService } from 'src/model/trader-service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.css']
})
export class AnalysisComponent implements OnInit {

  service: TraderService
  stocks: string[]
  curStock="select a stock here"
  LineChart = [];
  pricePoints: stockPrices[]
  duration = 4
  allDurations = [0.5, 1, 2, 3, 4, 5, 6, 7]

  constructor(service: TraderService) {
    this.service = service;
    service.subscribePrice(this);
    service.notifyPrice(this.curStock);
  }

  ngOnInit() {
  }

  latestPrice(prices: Array<stockPrices>) {
    this.pricePoints = prices;
  }

  latestStocks(stocks: Array<string>) {
    this.stocks = stocks;
  }

  selectStock(event){
    this.curStock = event.target.value
    this.service.notifyPrice(this.curStock);
  }

  selectDuration(event){
    this.duration = event.target.value
  }

  displayPoints(){
    return this.duration *240
  }

  createChart() {
    let curPrices = this.pricePoints.map(stockPrices => stockPrices.price)
    let curTimeStamps = this.pricePoints.map(stockPrices => stockPrices.when)
    if (curPrices.length >= this.displayPoints()){
      curPrices = curPrices.slice(curPrices.length-this.displayPoints(), curPrices.length)
      curTimeStamps = curTimeStamps.slice(curTimeStamps.length-this.displayPoints(), curTimeStamps.length)
    }
  
    this.LineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: curTimeStamps,
        datasets: [{
          label: 'Closing Prices',
          data: curPrices,
          fill: false,
          lineTension: 0.4,
          borderColor: "black",
          borderWidth: 2
        }]
      },
      options: {
        title: {
          text: "Price Trend for " + this.curStock,
          display: true
        },
        scales: {
          // yAxes: [{
          //   ticks: {
          //     suggestedMin: Math.min.apply(this.pricePoints)-5,
          //     suggestedMaz: Math.max.apply(this.pricePoints)+5,
          //   }
          // }],
          xAxes: [{
            type: 'time',
            ticks: {
              // display: false
              autoSkip: true,
              maxTicksLimit: 10
            }
          }]

        }
      }
    });
  }

}
