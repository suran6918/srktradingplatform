import { Component, Output,Input, SimpleChanges, SimpleChange, Injectable } from "@angular/core";
import { Trader } from "../model/trader";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { TraderService } from "../model/trader-service";
import { TraderUpdate } from "../model/trader-service";
import { DatashareService } from 'src/view/datashare.service';
import {Sort} from '@angular/material/sort';
import {EventEmitter} from "@angular/core";

/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-table",
  templateUrl: "./trader-table.html",
  styleUrls: ["./trader-table.css"]
})
export class TraderTable implements TraderUpdate {

  tableTitle: string;
  service: TraderService;
  traders: Array<Trader> = [];
  displayedTraders: Array<Trader> = [];
  stateFilter: string = "anyState";
  stockFilter: string = "anyStock";
  initDisplay: boolean = true;
  currentSort: Sort = null;
  stocks: string[] = [];
  @Output() stocksChange:  EventEmitter<string[]> = new EventEmitter();
  @Input() filterEvent;
  @Input() specificType:string;
  

  ngOnChanges(changes: SimpleChanges){
    const filter: SimpleChange = changes.filterEvent;
    if(filter.previousValue!=filter.currentValue){
      if(this.filterEvent.type=="stock")
        this.stockFilter = this.filterEvent.filter;
      else
        this.stateFilter = this.filterEvent.filter;
      this.applyFilter();
    }
  }

  populateStocks(){
    this.traders.forEach(trader=>{
      if(!this.stocks.includes(trader.stock)){
        this.stocks.push(trader.stock);
      }
    })
  }

  /**
   * Helper to format times in mm:ss format.
   */
  minSec(millis: number): string {
    let seconds = millis / 1000;
    const minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    const pad = seconds < 10 ? "0" : "";
    return "" + minutes + ":" + pad + seconds;
  }

  /**
   * Store the injected service references.
   */
  constructor(service: TraderService, private data: DatashareService) {
    this.service = service;
    service.subscribe(this);
    service.notify();

  }
  ngOnInit(){
    switch(this.specificType){
      case "2MA":
        this.tableTitle = "Two Moving Average"
        break
      case "BB":
        this.tableTitle="Bollinger Bands"
        break
      case "All":
        this.tableTitle="All Types"
        break
    }
  }

  /**
   * Set history data upon user click on History button. 
   */
  passHistory(t : Trader){
    let pos = t.positions
    this.data.onGetHistory(pos);
  }


  /**
   * Replace our array of traders with the latest,
   * which will triger a UI update.
   */
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
    this.applyFilter();
    this.stocks = [];
    this.populateStocks();
    this.stocksChange.emit(this.stocks);
    if(this.initDisplay){
      this.displayedTraders = this.traders.slice(0);
      this.filterByStrategy()
      this.initDisplay = false;
    }
  }

  /**
   * Helper to derive a label for the trader's state: "Started",
   * "Stopped", or, if deactivated but still closing out a position,
   * "Stopping".
   */
  getState(trader: Trader) {
    if(trader.stopping){
      if(document.getElementById("spin"+trader.ID)){
        document.getElementById("spin"+trader.ID).className+="fa fa-spinner fa-spin fa-1x fa-fw";
      }
      
      return "Stopping";
    }
      
    return trader.active ? "Started" : "Stopped";
  }

  /**
   * Helper to derive the total number of trades made by this trader.
   */
  getTotalTrades(): number {
    return this.displayedTraders.map(t => t.trades).reduce((x, y) => x + y, 0);
  }

  /**
   * Helper to derive the trader's total profit.
   */
  getTotalProfit(): number {
    return this.displayedTraders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
  }

  /**
   * Finds the trader by trader ID and uses the
   * TraderService component to send an HTTP request to toggle the
   * trader's state.
   */
  startOrStop(ev: any, hardStop: boolean) {
    const index = ev.target.id;
    const trader = this.displayedTraders.find(trader=> {return trader.ID==index});
    this.service.setActive(trader.ID, !trader.active);
  }

  /**
   * Applies all the current filters and updates the 
   * traders on display.
   */
  applyFilter(){
    this.displayedTraders = this.traders.slice(0);
    let active:boolean = this.stateFilter=='active'? true: false;
    this.filterByActive(active);
    this.filterByStock(this.stockFilter);
    this.filterByStrategy();
    if(this.currentSort)
      this.sortData(this.currentSort);
  }

  /**
   * Filter traders on display by active/inactive. 
  */
  filterByActive(active: boolean){
    if(this.stateFilter=="anyState")
      return;
    this.displayedTraders = this.displayedTraders.filter(trader=>trader.active == active)
  }

  /**
   * Filter traders on display by stock name. 
  */
  filterByStock(stock: string){
    if(this.stockFilter=="anyStock")
      return;
    this.displayedTraders = this.displayedTraders.filter(trader=>trader.stock == stock)
  }

  /**
   * Filter traders on display by startegy. 
  */
  filterByStrategy(){
    this.displayedTraders = this.displayedTraders.filter(trader=>trader['@type']===this.specificType)
  }

  /**
   * Compare values of trader properties.
  */
  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  /**
   * Orders traders on display by the given column.  
   */
  sortData(sort: Sort) {
    this.currentSort = sort;
    const data = this.displayedTraders.slice();
    if (!sort.active || sort.direction === '') {
       return;
    }
    this.displayedTraders = data.sort((a:Trader, b:Trader) => {
       const isAsc = sort.direction === 'asc';
       switch (sort.active) {
          case 'size': return this.compare(a.size, b.size, isAsc);
          case 'profit': return this.compare(a.profitOrLoss, b.profitOrLoss, isAsc);
          case 'ROI': return this.compare(a.ROI, b.ROI, isAsc);
          case 'trades': return this.compare(a.trades, b.trades, isAsc);
          default: return 0;
       } 
    });
  }
  
}
