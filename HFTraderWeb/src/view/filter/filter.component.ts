import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { TraderService } from "../../model/trader-service";
import { DatashareService } from '../datashare.service';

/**
 * The filter component that allows users to filter traders to be displayed 
 * on the main page. 
 */
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent{

  @Input() stocks;
  @Output() filterApplied = new EventEmitter();
  @Output() strategyCheck = new EventEmitter();
  selectedStateFilter: string = "anyState";
  selectedStockFilter: string = "anyStock";

  /**
   * Emit strategyCheck event upon user click.
   */
  selectStrategy(strategyName: string){
    this.strategyCheck.emit(strategyName);
  }

  
  /**
   * Emit filterApplied event check event upon user select.
   */
  selectFilter(event, filter: string, type: string){
    this.filterApplied.emit({filter: filter, type: type});
  }
}
