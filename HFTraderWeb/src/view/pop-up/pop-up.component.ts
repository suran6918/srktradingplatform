import { Component, OnInit, Input, ElementRef, Output, ɵConsole } from '@angular/core';
import { ModalService } from '../modal-service';
import { Position } from '../../model/position';
import { DatashareService } from '../datashare.service';
import { Observable, Subscription } from 'rxjs';

/**
 * The History button popup component that displays the trading histories of traders.
 */
@Component({
  selector: 'app-popup',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})
export class PopUpComponent implements OnInit {
  
  positions : Position[]
  csvInfo: []
  constructor(private popup: ModalService, private dataService: DatashareService) {
    
  }

  ngOnInit() {
   this.dataService.historyEventEmitter.subscribe((result) => {
      this.positions = result
      this.openPopupWindow('history')
      this.setModalSize(this.positions.length) 
    })
  }

  /**
   * Set the size of the history modal. 
   */
  setModalSize(size:number){
    // let base_height = 20
    let new_modal_height = 20 + (5*size)
    if(new_modal_height>70){
      new_modal_height=70
    }
    document.getElementById('historyTable').style.height= new_modal_height+'vh';
  }

  /**
   * Open popup for the given user.
   */
  openPopupWindow(id){
    this.popup.open(id)
    
  }

  /**
   * Close popup for the given user.
   */
  closePopupWindow(id){
    this.popup.close(id)
  }

  stockData = [  
    {
        Symbol: "AAPL",
        Company: "Apple Inc.",
        Price: 132.54
    },
    {
        Symbol: "INTC",
        Company: "Intel Corporation",
        Price: 33.45
    },
    {
        Symbol: "GOOG",
        Company: "Google Inc",
        Price: 554.52
    },
  ];

  /**
   * Convert position objects into correct format.
   */
  convertDataToCorrectFormat(){
    let position
    var temp_info = []
    for(position in this.positions){
      let current_position = this.positions[position];
      temp_info.push( 
         {
           Symbol: current_position.openingTrade.stock,
           Size: current_position.openingTrade.size,
           Opening_action: current_position.openingTrade.buy? 'buy':'sell',
           Opening_price: current_position.openingTrade.price,
           Closing_action: current_position.closingTrade===null? ' ': current_position.closingTrade.buy? 'buy':'sell',
           Closing_price: current_position.closingTrade===null? ' ' : current_position.closingTrade.price,
           Profit_or_Loss: current_position.profitOrLoss,
          //  ROI: current_position.ROI
          }
          )
    }
    this.stockData = temp_info;
  }

  /**
   * Convert formatted data into strings of csv format.
   */
  convertArrayOfObjectsToCSV(args) {  
    let result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);
    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
}

/**
 * Convert trading history into csv file and enable its download.  
 */
 downloadCSV() { 
  this.convertDataToCorrectFormat();
  var data, link;
  var csv = this.convertArrayOfObjectsToCSV({
      data: this.stockData
  });
  console.log(csv);
  if (csv == null) return;
  var filename = 'HistoryData:' + new Date();
  filename = filename+'.csv' || 'export.csv';

  if (!csv.match(/^data:text\/csv/i)) {
      csv = 'data:text/csv;charset=utf-8,' + csv;
  }
  data = encodeURI(csv);

  link = document.createElement('a');
  link.setAttribute('href', data);
  link.setAttribute('download', filename);
  link.click();
}
}
