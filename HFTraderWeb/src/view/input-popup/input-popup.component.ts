import { Component, OnInit } from '@angular/core';
import { ModalService } from '../modal-service';
import { DatashareService } from '../datashare.service';
import { TraderService } from 'src/model/trader-service';
import { TwoMovingAverages } from 'src/model/two-moving-averages';
import { BollingerBands } from 'src/model/bollinger-bands';
import { FormGroup, FormControl, Validators  } from '@angular/forms';

/**
 * The new trader creation popup component that allows user
 * to create new traders with differernt configurations.
 */
@Component({
  selector: 'app-input-popup',
  templateUrl: './input-popup.component.html',
  styleUrls: ['./input-popup.component.css']
})
export class InputPopupComponent implements OnInit {

  service: TraderService;
  type: string;
  stock: string;
  size: number;
  lengthShort: number;
  lengthLong: number;
  exitThreshold: number;
  length:number;
  multiple:number;
  validationMsg:string

  
  constructor(private popUpService: ModalService,  private dataService: DatashareService, service: TraderService) {
    this.service = service;
    this.type = "2MA";
    this.stock = "MRK";
    this.size = 1000;
    this.lengthShort = 30;
    this.lengthLong = 60;
    this.exitThreshold = 3;
    this.length = 180; // 12 periods (12*15=180)
    this.multiple=2;
   }

   myValidataion: FormGroup;
   stockNameValidator: FormControl;
   stockSizeValidator: FormControl;
 
  ngOnInit() {
    this.createFromControls();
    this.createForm();
    this.dataService.inputGroupEventEmitter.subscribe((result) => {
      this.openPopupWindow('inputGroup')
    })
  }

  /**
   * Create formControl objects for the form.
   */
  createFromControls(){
    this.stockNameValidator = new FormControl('', [Validators.required, Validators.pattern("[a-zA-Z]*")]);
    this.stockSizeValidator = new FormControl('', [Validators.required, Validators.pattern("[0-9]*")]);
  }

  createForm(){
    this.myValidataion = new FormGroup({stockName: this.stockNameValidator, stockSize:this.stockSizeValidator});
  }

  /**
   * Reads the values of form controls via two-way binding.
   * Creates an instance of the trader (only 2MA traders currently supported)
   * and sends it to the server to be created and activated.
   */
  create() {
    //check if stock size is valid
    let checkSize = Number(this.size)
    if (isNaN(checkSize)){
      this.size = 1000;
      console.log("this size is invalid")
      this.validationMsg = "Invalid stock size: can only input integer value!"
      this.openPopupWindow('validation')
      return
    }

    // check if stock name is valid
    var hasNumber = /\d/;
    if (hasNumber.test(this.stock)){
      this.stock = "MRK";
      console.log("this stock name is invalid")
      this.validationMsg = "Invalid stock name: can only input characters"
      this.openPopupWindow('validation')
      return
    }

    // create trader
    if(this.type=="2MA"){
      this.service.createTrader(new TwoMovingAverages
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.lengthShort * 1000, this.lengthLong * 1000, this.exitThreshold / 100));
    }
    else if(this.type=="BB"){
      this.service.createTrader(new BollingerBands
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.length * 1000, this.multiple, this.exitThreshold / 100));
    }
  }

  /**
   * Open popup window upon user click. 
   */
  openPopupWindow(id){
    this.popUpService.open(id)
  }

  /**
   * Close popup window upon user click. 
   */
  closePopupWindow(id){
    this.popUpService.close(id)
  }

}
