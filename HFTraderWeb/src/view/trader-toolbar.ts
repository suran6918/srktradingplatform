import { Component } from "@angular/core";
import { Trader } from "../model/trader";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { BollingerBands } from "../model/bollinger-bands";

import { TraderService } from "../model/trader-service";

/**
 * Angular component for a toolbar that allows the user to configure
 * and to create new traders. The component logic and the HTML template
 * currently support only the 2MA trader type.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-toolbar",
  templateUrl: "./trader-toolbar.html",
  styleUrls: ["./trader-toolbar.css"]
})
export class TraderToolbar {

  service: TraderService;
  type: string;
  stock: string;
  size: number;
  lengthShort: number;
  lengthLong: number;
  exitThreshold: number;
  length:number;
  multiple:number;
  
  /**
   * Set default values for all properties, which will flow out to the
   * initial UI via two-way binding.
   */
  constructor(service: TraderService) {
    this.service = service;

    this.type = "2MA";
    this.stock = "MRK";
    this.size = 1000;
    this.lengthShort = 30;
    this.lengthLong = 60;
    this.exitThreshold = 3;
    this.length = 180; // 12 periods (12*15=180)
    this.multiple=2;
  }

  /**
   * Reads the values of form controls via two-way binding.
   * Creates an instance of the trader (only 2MA traders currently supported)
   * and sends it to the server to be created and activated.
   */
  create() {
    if(this.type=="2MA"){
      this.service.createTrader(new TwoMovingAverages
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.lengthShort * 1000, this.lengthLong * 1000, this.exitThreshold / 100));
    }
    else if(this.type=="BB"){
      this.service.createTrader(new BollingerBands
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.length * 1000, this.multiple, this.exitThreshold / 100));
    }
  }
}
