import { Injectable, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { Subject, Observable } from 'rxjs';
import { Position }  from '../model/position'

@Injectable({
  providedIn: 'root'
})

/**
 * Servive component that supports data transmission among child components. 
 */
export class DatashareService {

  data: {}
  constructor() {
   }

  @Output()
  historyEventEmitter : EventEmitter<Position[]>= new EventEmitter()

  @Output()
  inputGroupEventEmitter : EventEmitter<string>= new EventEmitter()

  onGetHistory(pos){
    this.historyEventEmitter.emit(pos);
    return pos;
  }

  onGetInputGroup(id){
    this.inputGroupEventEmitter.emit(id);
    return id;
  }


  setData(id, value){
    this.data[id] = value;
  }

  getData(id){
    return this.data[id];
  }
}
