import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  activeValue:string;
  constructor() { }

  ngOnInit() {
  }

  /**
   * Change the view on display.
   */
  changeActive(newval:string){
    this.setAllInactive()
    document.getElementById(newval).classList.add("active");
  }

  /**
   * Remove all views on display.
   */
  setAllInactive(){
    document.getElementById('home').classList.remove("active")
    // document.getElementById('contactUs').classList.remove("active")
    document.getElementById('analysis').classList.remove("active")

  }
}
