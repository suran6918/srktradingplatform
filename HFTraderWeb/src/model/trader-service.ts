import { Injectable } from "@angular/core";
import { Trader } from "./trader";
import { TwoMovingAverages } from "./two-moving-averages";
import { BollingerBands } from "./bollinger-bands";
import { stockPrices } from './stockPrices';

/**
 * Subscriber interface for receiving updates on current traders.
 *
 * @author Will Provost
 */
export interface TraderUpdate {
  latestTraders(traders: Array<Trader>);
}

export interface PriceUpdate {
  latestPrice(prices: Array<stockPrices>);
  latestStocks(stocks: Array<string>);
}

/**
 * A client component for the HFTrader application's TraderService.
 * Also has the responsibility to set a timer and to poll the service
 * for updates on traders and their status and trading history.
 * Subscribe with an implementation of the above TraderUpdate interface,
 * and this component will push updates as it fetches them.
 *
 * @author Will Provost
 */
@Injectable({
  providedIn: 'root',
})
export class TraderService {

  URL: string;
  subscribers: Array<TraderUpdate>;
  priceSubscribers: Array<PriceUpdate>;
  timer: any;

  /**
   * Set up service URL. Bind all methods so our 'this' references
   * make sense. Initialize subscribers array. Start the polling timer.
   */
  constructor() {
    // uncomment this line during development!
    // this.URL = "http://localhost:8082/traders";

    //url for when we are in production
    this.URL = "/traders";

    this.handler = this.handler.bind(this);
    this.checkResponseCode = this.checkResponseCode.bind(this);
    this.checkOK = this.checkOK.bind(this);
    this.checkCreated = this.checkCreated.bind(this);
    this.getTraders = this.getTraders.bind(this);
    this.getTrader = this.getTrader.bind(this);
    this.setActive = this.setActive.bind(this);
    this.createTrader = this.createTrader.bind(this);
    this.getPrices = this.getPrices.bind(this);

    this.subscribers = [];
    this.priceSubscribers = [];
    this.start();
  }

  /**
   * Add the subscriber.
   */
  subscribe(subscriber: TraderUpdate) {
    this.subscribers.push(subscriber);
  }

  subscribePrice(subscriber: PriceUpdate) {
    this.priceSubscribers.push(subscriber);
  }

  /**
   * Remove the subscriber.
   */
  unsubscribe(subscriber: TraderUpdate) {
    for (let i = 0; i < this.subscribers.length; ++i) {
      if (this.subscribers[i] === subscriber) {
        this.subscribers.splice(i, 1);
        break;
      }
    }
  }

  unsubscribePrice(subscriber: PriceUpdate) {
    for (let i = 0; i < this.priceSubscribers.length; ++i) {
      if (this.priceSubscribers[i] === subscriber) {
        this.priceSubscribers.splice(i, 1);
        break;
      }
    }
  }

  /**
   * Call each subscriber, passing the current traders array.
   */
  notify() {
    this.getTraders().then(traders => {
      for (const subscriber of this.subscribers) {
        subscriber.latestTraders(traders);
      }
    });
  }

  /**
   * Update the lastest prices for each subscriber. 
   */
  notifyPrice(stock: string) {
    this.getPrices(stock).then(prices => {
      for (const subscriber of this.priceSubscribers) {
        subscriber.latestPrice(prices);
      }
    });
    this.getStocks().then(stocks => {
      for (const subscriber of this.priceSubscribers) {
        subscriber.latestStocks(stocks);
      }
    });
  }

  /**
   * Set a timer to go off in one second and then every 15 seconds therafter,
   * calling our notify() method each time.
   */
  start() {
    const oneSecond = setInterval(() => {
      this.notify();
      this.timer = setInterval(this.notify.bind(this), 15000);
      clearInterval(oneSecond);
    }, 1000);
  }

  /**
   * Shut down the polling timer.
   */
  stop() {
    clearInterval(this.timer);
  }

  /**
   * Error handler currently just logs to the console.
   * A popup message box or some other UI should come into play at some point.
   */
  handler(err): any {
    console.log(err);
    return null;
  }

  /**
   * Helper to check that the HTTP response code was the expected alue.
   * Either throws an error or returns the response, making the function
   * suitable for use in a fetch/then chain.
   */
  checkResponseCode(response: Response, expected: number): Response {
    if (response.status !== expected) {
      throw Error("Unexpected response code: " + response.status);
    }
    return response;
  }

  /**
   * Specialization of checkResponseCode() that expects HTTP 200 OK.
   */
  checkOK(response: Response): Response {
    return this.checkResponseCode(response, 200);
  }

  /**
   * Specialization of checkResponseCode() that expects HTTP 201 Created.
   */
  checkCreated(response: Response): Response {
    return this.checkResponseCode(response, 201);
  }

  /**
   * Parses the given (weakly typed) object and creates the appropriate
   * type of Trader, holding the appropriate values.
   * Currently, only 2MA traders are supported.
   */
  static makeTrader(source: any): Trader {
    if (source["@type"] === "2MA") {
      return new TwoMovingAverages(source.id, source.stock, source.size,
        source.active, source.stopping,
        source.positions, source.profitOrLoss, source.roi,
        source.lengthShort, source.lengthLong, source.exitThreshold);
    } else if (source["@type"] === "BB") {
      return new BollingerBands(source.id, source.stock, source.size,
        source.active, source.stopping,
        source.positions, source.profitOrLoss, source.roi,
        source.length, source.multiple, source.exitThreshold);
    } else {
      throw Error("Unknown trader type: " + source["@type"]);
    }
  }

  static makePricePoint(source: any): stockPrices {
    return new stockPrices(source.stock, source.close, source.timestamp)
  }

  /**
   * Calls the HTTP operation and returns a Promise bearing an array of
   * Trader objects.
   */
  getTraders(): Promise<Array<Trader>> {
    return fetch(this.URL)
      .then(this.checkOK)
      .then(response => response.json())
      .then(traders => traders.map(TraderService.makeTrader))
      .catch(this.handler);
  }

    /**
   * Calls the HTTP operation and returns a Promise bearing an array of
   * prices
   */
  getPrices(stock: string): Promise<Array<stockPrices>> {
    return fetch(this.URL + "/stock/" + stock)
      .then(this.checkOK)
      .then(response => response.json())
      .then(prices => prices.map(TraderService.makePricePoint))
      .catch(this.handler);
  }

     /**
   * Calls the HTTP operation and returns a Promise bearing an array of
   * prices
   */
  getStocks(): Promise<Array<string>> {
    return fetch(this.URL + "/allstock/")
      .then(this.checkOK)
      .then(response => response.json())
      .catch(this.handler);
  }

  /**
   * Calls the HTTP operation and returns a Promise bearing the
   * requested trader object.
   */
  getTrader(ID: number): Promise<Trader> {
    return fetch(this.URL + "/" + ID)
      .then(this.checkOK)
      .then(response => response.json())
      .then(TraderService.makeTrader)
      .catch(this.handler);
  }

  /**
   * Calls the HTTP operation, and on successful response triggers
   * a notify() which in turn will fetch an updated list of traders.
   */
  setActive(ID: number, start: boolean) {
    return fetch(this.URL + "/" + ID + "/active", {
      method: "PUT",
      body: start ? "true" : "false"
    })
      .then(this.checkOK)
      .then(response => { this.notify(); return response; })
      .catch(this.handler);
  }

  /**
   * Calls the HTTP operation and returns a Promise bearing
   * the newly created trader, which will carry the server-generated ID.
   */
  createTrader(trader: Trader): Promise<Trader> {
    return fetch(this.URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(trader)
    })
      .then(this.checkCreated)
      .then(response => response.json())
      .then(TraderService.makeTrader)
      .then(created => { this.notify(); return created; })
      .catch(this.handler);
  }
}
