import { Timestamp } from 'rxjs';

export class stockPrices {

    stock: string;
    price: number;
    when: string;

    constructor(s: string, p: number, w :string){
        this.stock = s;
        this.price = p;
        this.when = w;
    }
}