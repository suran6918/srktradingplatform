import { Position } from "./position";
import { Trader } from "./trader";

/**
 * Serializable/deserializable encapsulation of a 2MA strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Sheheryar Butt
 */
export class BollingerBands extends Trader {
  length: number;
  multiple: number;
  exitThreshold: number;

  constructor(ID: number, stock: string, size: number, active: boolean,
      stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
      length: number, multiple: number,  exitThreshold: number) {
  
    super("BB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
    this.length = length;
    this.multiple = multiple;
    this.exitThreshold = exitThreshold;
  }
}
